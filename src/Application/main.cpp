#include <memory>
#include <iostream>

#include "../Models/Common/Person.h"
#include "../Dao/Common/PersonDAO.h"
#include "../Dao/Account/UserDAO.h"
#include "../Dao/Engine/Connection.h"

#include "../Services/Server/Server.h"

using namespace std;
using namespace DAO;

using namespace Models::Commons;

int main(int argc, char** argv){
	try{
	    Http::Server::server server("127.0.0.1", "8080", "./src/Front");
    	server.run();
  	} catch (std::exception& e) {
    	std::cerr << "exception: " << e.what() << "\n";
  	}
	return 0;
}
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ctime>
#include <vector>
#include <string>
#include <random>
#include <iostream>
#include <algorithm>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <Test/Utils.h>
#include <Services/Rest/Engine/HttpHeaderParser.h>

using std::cout                          ;
using std::endl                          ;
using std::find                          ;
using std::string                        ;
using std::vector                        ;
using std::to_string                     ;
using boost::any_cast                    ;
using Http::Server::header               ;
using Rest::HttpHeaderParser             ;
using boost::posix_time::ptime           ;
using Test::generate_random_int          ;
using Test::generate_random_bool         ;
using Test::generate_random_ptime        ;
using Test::generate_random_alpha        ;
using Test::generate_random_float        ;
using Test::generate_random_alpha_num    ;
using boost::posix_time::to_iso_string   ;
using boost::posix_time::time_from_string;

BOOST_AUTO_TEST_SUITE(HttpHeaderParserTest)

BOOST_AUTO_TEST_CASE(Creation_parsing_int){
	SVptr <header> headers     ;
	SVptr <int   > params_vals ;
	SVptr <string> params_names;

	unsigned int params_count      = generate_random_int(100);
	unsigned int params_max_lenght = generate_random_int(100);
	
	for(int i = 0; i < params_count; ++i){
		string header_name;
		params_vals->push_back(generate_random_int());
		
		do{
			header_name = generate_random_alpha(generate_random_int(params_max_lenght));
		}while(find(params_names->begin(), params_names->end(), header_name) != params_names->end());

		params_names->push_back(header_name);
		headers     ->push_back(header("rest_param_"+params_names->back()+"_int", to_string(params_vals->back())));
	}

	auto params = HttpHeaderParser(headers).getParameters();
	
	if(params.size() != params_count){
		cout<<params_count<<" | "<<params.size()<<endl;
		for(int i = 0; i < params_count; ++i)
			cout<<params_names->at(i)<<" | "<<params_vals->at(i)<<endl;
		BOOST_FAIL("Parsed parameters count don't match");
	}

	try{
		for(int i = 0; i < params_count; ++i)
			BOOST_REQUIRE(any_cast<int>(params.at(params_names->at(i))) == params_vals->at(i));
	}catch(std::exception& e){
		BOOST_FAIL("Parsing header parameter failed");
	}
}

BOOST_AUTO_TEST_CASE(Creation_parsing_bool){
	SVptr <header> headers     ;
	SVptr <bool  > params_vals ;
	SVptr <string> params_names;

	unsigned int params_count      = generate_random_int(100);
	unsigned int params_max_lenght = generate_random_int(100);
	
	for(int i = 0; i < params_count; ++i){
		string header_name;
		params_vals->push_back(generate_random_int());
		
		do{
			header_name = generate_random_alpha(generate_random_int(params_max_lenght));
		}while(find(params_names->begin(), params_names->end(), header_name) != params_names->end());

		params_names->push_back(header_name);
		headers     ->push_back(header("rest_param_"+params_names->back()+"_bool", params_vals->back() == true ? "true" : "false"));
	}

	auto params = HttpHeaderParser(headers).getParameters();
	if(params.size() != params_count){
		cout<<params_count<<" | "<<params.size()<<endl;
		for(int i = 0; i < params_count; ++i)
			cout<<params_names->at(i)<<" | "<<params_vals->at(i)<<endl;
		BOOST_FAIL("Parsed parameters count don't match");
	}

	try{
		for(int i = 0; i < params_count; ++i)
			BOOST_REQUIRE(any_cast<bool>(params.at(params_names->at(i))) == params_vals->at(i));
	}catch(std::exception& e){
		BOOST_FAIL("Parsing header parameter failed");
	}
}

BOOST_AUTO_TEST_CASE(Creation_parsing_date){
	SVptr <header> headers     ;
	SVptr <ptime > params_vals ;
	SVptr <string> params_names;

	unsigned int params_count      = generate_random_int(100);
	unsigned int params_max_lenght = generate_random_int(100);
	
	for(int i = 0; i < params_count; ++i){
		string header_name;
		params_vals->push_back(generate_random_ptime());
		
		do{
			header_name = generate_random_alpha(generate_random_int(params_max_lenght));
		}while(find(params_names->begin(), params_names->end(), header_name) != params_names->end());

		params_names->push_back(header_name);
		headers     ->push_back(header("rest_param_"+params_names->back()+"_ptime", to_iso_string(params_vals->back())));
	}

	auto params = HttpHeaderParser(headers).getParameters();
	if(params.size() != params_count){
		cout<<params_count<<" | "<<params.size()<<endl;
		BOOST_FAIL("Parsed parameters count don't match");
	}

	try{
		for(int i = 0; i < params_count; ++i)
			BOOST_REQUIRE(any_cast<ptime>(params.at(params_names->at(i))) == params_vals->at(i));	
	}catch(std::exception& e){
		BOOST_FAIL("Parsing header parameter failed");
	}
}

BOOST_AUTO_TEST_CASE(Creation_parsing_string){
	SVptr <header> headers     ;
	SVptr <string> params_vals ;
	SVptr <string> params_names;

	unsigned int params_count      = generate_random_int(100);
	unsigned int params_max_lenght = generate_random_int(100);
	
	for(int i = 0; i < params_count; ++i){
		string header_name;
		params_vals->push_back(generate_random_alpha_num(params_count));
		
		do{
			header_name = generate_random_alpha(generate_random_int(params_max_lenght));
		}while(find(params_names->begin(), params_names->end(), header_name) != params_names->end());

		params_names->push_back(header_name);
		headers     ->push_back(header("rest_param_"+params_names->back()+"_string", params_vals->back()));
	}

	auto params = HttpHeaderParser(headers).getParameters();
	if(params.size() != params_count){
		cout<<params_count<<" | "<<params.size()<<endl;
		BOOST_FAIL("Parsed parameters count don't match");
	}

	try{
		for(int i = 0; i < params_count; ++i)
			BOOST_REQUIRE(any_cast<string>(params.at(params_names->at(i))) == params_vals->at(i));	
	}catch(std::exception& e){
		BOOST_FAIL("Parsing header parameter failed");
	}
}


BOOST_AUTO_TEST_CASE(Creation_parsing_float){
	SVptr <header> headers     ;
	SVptr <float > params_vals ;
	SVptr <string> params_names;

	unsigned int params_count      = generate_random_int(100);
	unsigned int params_max_lenght = generate_random_int(100);
	
	for(int i = 0; i < params_count; ++i){
		string header_name;
		params_vals->push_back(generate_random_float(params_count));
		
		do{
			header_name = generate_random_alpha(generate_random_int(params_max_lenght));
		}while(find(params_names->begin(), params_names->end(), header_name) != params_names->end());

		params_names->push_back(header_name);
		headers     ->push_back(header("rest_param_"+params_names->back()+"_float", to_string(params_vals->back())));
	}

	auto params = HttpHeaderParser(headers).getParameters();
	if(params.size() != params_count){
		cout<<params_count<<" | "<<params.size()<<endl;
		BOOST_FAIL("Parsed parameters count don't match");
	}

	try{
		for(int i = 0; i < params_count; ++i)
			BOOST_REQUIRE((any_cast<float>(params.at(params_names->at(i))) - params_vals->at(i)) < 0.0001);	
	}catch(std::exception& e){
		BOOST_FAIL("Parsing header parameter failed");
	}
}

BOOST_AUTO_TEST_SUITE_END()
#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <chrono>
#include <limits>

#include <boost/date_time/posix_time/posix_time.hpp>

namespace Test {

using std::string;
using std::numeric_limits;
using boost::posix_time::ptime          ;
using boost::posix_time::from_time_t    ;
using std::chrono::high_resolution_clock;

int generate_random_int(unsigned int max=1){
	std::srand(high_resolution_clock::now().time_since_epoch().count());
    auto rnd    = std::rand();
    auto cap    = max > numeric_limits<int>::max() ? numeric_limits<int>::max() : max;
    auto result = rnd % cap;
    return result > 0 ? result : result + 1;
}

int generate_random_bool(){
	std::srand(high_resolution_clock::now().time_since_epoch().count());
    return std::rand() % 2 ? true : false;
}

float generate_random_float(float max=1000000){
    std::srand(high_resolution_clock::now().time_since_epoch().count());
    auto result = (float)rand()/((float)numeric_limits<float>::max()/max);
    return result;
}

string generate_random_alpha(unsigned int len) {
    string result(len, 'a');
    static const char alphanum[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) 
        result[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    return result;
}

string generate_random_alpha_num(unsigned int len) {
	string result(len, 'a');
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) 
        result[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    return result;
}

ptime generate_random_ptime(){
  return from_time_t(generate_random_int(1410065408));
}

}
#endif
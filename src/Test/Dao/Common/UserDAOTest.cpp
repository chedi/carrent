#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE UserDAO
#include <boost/test/unit_test.hpp>

#include <Dao/Account/UserDAO.h>

using DAO::UserDAO   ;
using DAO::PersonDAO ;

BOOST_AUTO_TEST_SUITE(UserDAOTest)

BOOST_AUTO_TEST_CASE(personDAOCreation1){
	string original_nid        = "0001"  ;
	string original_last_name  = "l_name";
	string original_first_name = "f_name";
	string nid                 = original_nid;
	string last_name           = original_last_name;
	string first_name          = original_first_name;
	auto personDAO = make_shared<PersonDAO>(make_shared<Person>(nid, first_name, last_name));
	BOOST_CHECK(personDAO->getId    ()                 == 0                  );
    BOOST_CHECK(*personDAO->getModel()->getNid      () == original_nid       );
    BOOST_CHECK(*personDAO->getModel()->getLastName () == original_last_name );
    BOOST_CHECK(*personDAO->getModel()->getFirstName() == original_first_name);

	BOOST_CHECK(nid       .empty());
    BOOST_CHECK(last_name .empty());
    BOOST_CHECK(first_name.empty());
}

BOOST_AUTO_TEST_CASE(personDAOCreation2){
	auto personDAO = make_shared<PersonDAO>(make_shared<Person>("0001", "f_name", "l_name"));
	BOOST_CHECK(personDAO->getId   ()                  == 0       );
    BOOST_CHECK(*personDAO->getModel()->getNid      () == "0001"  );
    BOOST_CHECK(*personDAO->getModel()->getLastName () == "f_name");
    BOOST_CHECK(*personDAO->getModel()->getFirstName() == "l_name");
}

BOOST_AUTO_TEST_CASE(personDAOCreation3){
	auto nid    = make_shared<string>("0001"  );
	auto f_name = make_shared<string>("f_name");
	auto l_name = make_shared<string>("l_name");

	auto personDAO = make_shared<PersonDAO>(make_shared<Person>(nid, f_name, l_name));
	BOOST_CHECK(personDAO->getId   ()                 == 0      );
    BOOST_CHECK(personDAO->getModel()->getNid      () == nid    );
    BOOST_CHECK(personDAO->getModel()->getLastName () == l_name );
    BOOST_CHECK(personDAO->getModel()->getFirstName() == f_name );

    BOOST_CHECK(personDAO->getModel()->getNid      ().get() == nid   .get() );
    BOOST_CHECK(personDAO->getModel()->getLastName ().get() == l_name.get() );
    BOOST_CHECK(personDAO->getModel()->getFirstName().get() == f_name.get() );
}


BOOST_AUTO_TEST_SUITE_END()
#ifndef CLIENT_DAO_H
#define CLIENT_DAO_H

#include <Models/Client/Client.h>
#include <Dao/Common/PersonDAO.h>
#include <Dao/Engine/ModelDAOTemplate.h>

using boost::mpl::int_;
using Models::Clients::Client;

namespace DAO {
	typedef boost::fusion::map<
		boost::fusion::pair<boost::mpl::pair<PersonDAO, int_<0>>, shared_ptr<PersonDAO>>
	> ClientDAODependencies;

	typedef boost::fusion::map<
		boost::fusion::pair<boost::mpl::pair<PersonDAO, int_<0>>, function<shared_ptr<Person>()>>
	> ClientDAOModelDependencies;

	using ClientDAO = ModelDAOTemplate<Client, ClientDAODependencies, ClientDAOModelDependencies>;
}

#endif
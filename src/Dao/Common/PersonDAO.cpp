#include <Dao/Common/PersonDAO.h>

namespace DAO {

using DAO::DAOSerializer     ;
using Models::Commons::Person;

template<> const string PersonDAO::INSERT_QUERY             = "INSERT INTO persons(nid, first_name, last_name) VALUES ('$nid', '$first_name', '$last_name') returning id";
template<> const string PersonDAO::UPDATE_QUERY             = "UPDATE persons SET nid = '$nid', first_name = '$first_name', last_name = '$last_name' WHERE id = $id";
template<> const string PersonDAO::DELETE_QUERY             = "DELETE FROM persons WHERE id = $id";
template<> const string PersonDAO::GET_ALL_QUERY            = "SELECT p.* FROM persons p";
template<> const string PersonDAO::SERIALIZATION            = "{ \"id\": $id, \"nid\" : \"$nid\", \"first_name\" : \"$first_name\", \"last_name\" : \"$last_name\"}";
template<> const string PersonDAO::GET_BY_ID_QUERY          = "SELECT p.* FROM persons p WHERE p.id = $id";
template<> const string PersonDAO::GET_BY_ATTRIBUTES_QUERY  = "SELECT p.* FROM persons p WHERE p.first_name='$first_name' AND p.last_name='$last_name'"; 
template<> const string PersonDAO::GET_TABLE_CREATION_QUERY = "CREATE TABLE persons(id SERIAL PRIMARY KEY, nid VARCHAR NOT NULL, first_name VARCHAR NOT NULL, last_name VARCHAR NOT NULL)";

template<>
PersonDAO* PersonDAO::parseResult(query_row row){
	return new PersonDAO(
		make_shared<Person>(
			row["nid"       ].as<string>(), 
			row["first_name"].as<string>(),
			row["last_name" ].as<string>()),
		row["id"].as<int>());
}

template<>
void PersonDAO::initializeFunctors() {
	serializers_map = {
		{"id"        , DAOSerializer{[this](){ return to_string(id)           ; }}},
		{"nid"       , DAOSerializer{[this](){ return *(model->getNid      ()); }}},
		{"last_name" , DAOSerializer{[this](){ return *(model->getLastName ()); }}},
		{"first_name", DAOSerializer{[this](){ return *(model->getFirstName()); }}}};
}

}
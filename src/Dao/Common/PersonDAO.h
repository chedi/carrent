#ifndef PERSON_DAO_H
#define PERSON_DAO_H

#include <Models/Common/Person.h>
#include <Dao/Engine/ModelDAOTemplate.h>

namespace DAO {	
	using PersonDAO = ModelDAOTemplate<Models::Commons::Person> ;
}

#endif
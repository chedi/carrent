#ifndef USER_DAO_H
#define USER_DAO_H

#include <Models/Account/User.h>
#include <Dao/Common/PersonDAO.h>
#include <Dao/Engine/ModelDAOTemplate.h>

namespace DAO {

using boost::mpl::int_      ;
using Models::Accounts::User;

	typedef boost::fusion::map<
		boost::fusion::pair<boost::mpl::pair<PersonDAO, int_<0>>, shared_ptr<PersonDAO>>
	> UserDAODependencies;

	typedef boost::fusion::map<
		boost::fusion::pair<boost::mpl::pair<PersonDAO, int_<0>>, function<shared_ptr<Person>()>>
	> UserDAOModelDependencies;

	using UserDAO = ModelDAOTemplate<User, UserDAODependencies, UserDAOModelDependencies>;

}

#endif
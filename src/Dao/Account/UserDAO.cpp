#include <Dao/Account/UserDAO.h>
#include <Dao/Common/PersonDAO.h>

namespace DAO {

using std::to_string          ;
using boost::fusion::at_key   ;
using boost::fusion::make_pair;

template<> const string UserDAO::INSERT_QUERY             = "INSERT INTO users(mail, login, password, status, person_id) VALUES ('$mail', '$login', '$password', '$status', '$person_id') returning id";
template<> const string UserDAO::UPDATE_QUERY             = "UPDATE users SET mail = '$mail', login = '$login', password = '$password', status = '$status', person_id = '$person_id' WHERE id = $id";
template<> const string UserDAO::DELETE_QUERY             = "DELETE FROM users WHERE id = $id";
template<> const string UserDAO::GET_ALL_QUERY            = "SELECT u.* FROM users u";
template<> const string UserDAO::SERIALIZATION            = "{\"id\" : \"$id\",\"mail\" : \"$mail\",\"login\" : \"$login\",\"password\" : \"$password\" ,\"status\" : $status ,\"person\" : $person_id}";
template<> const string UserDAO::GET_BY_ID_QUERY          = "SELECT u.* FROM users u WHERE u.id = $id";
template<> const string UserDAO::GET_BY_ATTRIBUTES_QUERY  = "SELECT u.* FROM users u WHERE u.mail='$mail' AND u.login='$login' AND u.password='$password' AND u.status='$status' AND person_id=$person_id";
template<> const string UserDAO::GET_TABLE_CREATION_QUERY = "CREATE TABLE users(id SERIAL PRIMARY KEY, mail VARCHAR, login VARCHAR, password VARCHAR, status BOOLEAN, person_id INT NOT NULL REFERENCES persons(id))" ;

template<>
UserDAO* UserDAO::parseResult(query_row row){
	auto person       = make_shared<PersonDAO>(row["person_id"].as<int>());
	auto person_model = person->getModel();
	auto result = new UserDAO(
		make_shared<User>(
				person_model                                       ,
				make_shared<string>(row["mail"      ].as<string>()), 
				make_shared<string>(row["login"     ].as<string>()), 
				make_shared<string>(row["password"  ].as<string>()),
				                    row["status"    ].as<bool  >()),
		row["id"].as<int>());
	at_key<boost::mpl::pair<PersonDAO, int_<0>>>(result->dependencies) = person;
	return result;
}

template<>
void UserDAO::initializeFunctors() {
	serializers_map = {
		{"id"       , DAOSerializer{[this](){ return to_string(id)                            ; }}},
		{"mail"     , DAOSerializer{[this](){ return *(model->getMail    ())                  ; }}},
		{"login"    , DAOSerializer{[this](){ return *(model->getLogin   ())                  ; }}},
		{"status"   , DAOSerializer{[this](){ return   model->getStatus  () ? "true" : "false"; }}},
		{"password" , DAOSerializer{[this](){ return *(model->getPassword())                  ; }}},
		{"person_id", DAOSerializer{
			[this](){ return synchronizeDependency<boost::mpl::pair<PersonDAO, int_<0>>>(false); },
			[this](){ return synchronizeDependency<boost::mpl::pair<PersonDAO, int_<0>>>(true ); }
		}}};

	modelDependencies = UserDAOModelDependencies(
		make_pair<boost::mpl::pair<PersonDAO, int_<0>>>(
			[this](){ return model->getPerson() ;}
	));
}

}
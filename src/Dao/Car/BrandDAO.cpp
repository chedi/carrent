#include <Dao/Car/BrandDAO.h>

namespace DAO  {

using Models::Cars::Brand;

template<> const string BrandDAO::INSERT_QUERY             = "INSERT INTO brands(model, manufacturer) VALUES ('$model', '$manufacturer');";
template<> const string BrandDAO::UPDATE_QUERY             = "UPDATE brands SET model = '$model', manufacturer = '$manufacturer' WHERE id = $id;";
template<> const string BrandDAO::DELETE_QUERY             = "DELETE FROM brands WHERE id = $id;";
template<> const string BrandDAO::GET_ALL_QUERY            = "SELECT b.* FROM brands b";
template<> const string BrandDAO::SERIALIZATION            = "{ \"id\" : $id, \"model\" : \"$model\", \"manufacturer\" : \"$manufacturer\" }";
template<> const string BrandDAO::GET_BY_ID_QUERY          = "SELECT b.* FROM brands b WHERE b.id = $id;";
template<> const string BrandDAO::GET_BY_ATTRIBUTES_QUERY  = "SELECT b.* FROM brands b WHERE b.model = '$model' AND b.manufacturer = '$manufacturer'";
template<> const string BrandDAO::GET_TABLE_CREATION_QUERY = "CREATE TABLE brands(id SERIAL PRIMARY KEY, manufacturer VARCHAR NOT NULL, model VARCHAR NOT NULL)";

template<>
BrandDAO* BrandDAO::parseResult(query_row row){
	return new BrandDAO(
		make_shared<Brand>(
			row["model"       ].as<string>() , 
			row["manufacturer"].as<string>()),
		row["id"].as<int>());
}

template<>
void BrandDAO::initializeFunctors() {
	serializers_map = {
		{"id"           , DAOSerializer{[this](){ return to_string(id)              ; }}},
		{"model"        , DAOSerializer{[this](){ return *(model->getModel       ()); }}},
		{"manufacturer" , DAOSerializer{[this](){ return *(model->getManufacturer()); }}}};
}

}
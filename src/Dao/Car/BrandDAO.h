#ifndef BRAND_DAO_H
#define BRAND_DAO_H

#include <Models/Car/Brand.h>
#include <Dao/Engine/ModelDAOTemplate.h>

namespace DAO {	
	using BrandDAO = ModelDAOTemplate<Models::Cars::Brand>;
}

#endif
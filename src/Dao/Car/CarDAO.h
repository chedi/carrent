#ifndef CAR_DAO_H
#define CAR_DAO_H

#include <memory>

#include <Models/Car/Car.h>
#include <Dao/Car/BrandDAO.h>
#include <Dao/Engine/ModelDAOTemplate.h>

namespace DAO  {

using boost::mpl::int_   ;
using Models::Cars::Car  ;
using Models::Cars::Brand;

	typedef boost::fusion::map<
		boost::fusion::pair<boost::mpl::pair<BrandDAO, int_<0>>, shared_ptr<BrandDAO>>
	> CarDAODependencies;

	typedef boost::fusion::map<
		boost::fusion::pair<boost::mpl::pair<BrandDAO, int_<0>>, function<shared_ptr<Brand>()>>
	> CarDAOModelDependencies;

	using CarDAO = ModelDAOTemplate<Car, CarDAODependencies, CarDAOModelDependencies>;

}
#endif
#include "CarDAO.h"

namespace DAO {

using std::to_string          ;
using boost::fusion::at_key   ;
using boost::fusion::make_pair;

template<> const string CarDAO::INSERT_QUERY             = "INSERT INTO cars(serial, brand_id) VALUES ('$serial', $brand_id);";
template<> const string CarDAO::UPDATE_QUERY             = "UPDATE cars SET serial = '$serial', brand_id = $brand_id WHERE id = $id;";
template<> const string CarDAO::DELETE_QUERY             = "DELETE FROM cars WHERE id = $id;";
template<> const string CarDAO::GET_ALL_QUERY            = "SELECT c.* FROM cars c";
template<> const string CarDAO::SERIALIZATION            = "{ \"id\" : $id, \"serial\" : \"$serial\", \"Brand\" : $brand_id";
template<> const string CarDAO::GET_BY_ID_QUERY          = "SELECT c.* FROM cars c WHERE c.id = $id;";
template<> const string CarDAO::GET_BY_ATTRIBUTES_QUERY  = "SELECT c.* FROM cars c WHERE c.serial = '$serial' AND c.brand_id = $brand_id";
template<> const string CarDAO::GET_TABLE_CREATION_QUERY = "CREATE TABLE cars(id SERIAL PRIMARY KEY, serial VARCHAR NOT NULL, brand_id INT NOT NULL REFERENCES brands(id))";

template<>
CarDAO* CarDAO::parseResult(query_row row){
	auto brand       = make_shared<BrandDAO>(row["brand_id"].as<int>());
	auto brand_model = brand->getModel();
	auto result = new CarDAO(
		make_shared<Car>(
				brand_model                                     ,
				make_shared<string>(row["serial"].as<string>())), 
		row["id"].as<int>());
	at_key<boost::mpl::pair<BrandDAO, int_<0>>>(result->dependencies) = brand;
	return result;
}

template<>
void CarDAO::initializeFunctors() {
	serializers_map = {
		{"id"      , DAOSerializer{[this](){ return to_string(id)        ; }}},
		{"serial"  , DAOSerializer{[this](){ return *(model->getSerial()); }}},
		{"brand_id", DAOSerializer{
			[this](){ return synchronizeDependency<boost::mpl::pair<BrandDAO, int_<0>>>(false); },
			[this](){ return synchronizeDependency<boost::mpl::pair<BrandDAO, int_<0>>>(true ); }
		}}};

	modelDependencies = CarDAOModelDependencies(
		make_pair<boost::mpl::pair<BrandDAO, int_<0>>>(
			[this](){ return model->getBrand() ;}
	));
}

}
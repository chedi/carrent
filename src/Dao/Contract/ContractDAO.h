#ifndef CONTRACT_DAO_H
#define CONTRACT_DAO_H

#include <Dao/Client/ClientDAO.h>
#include <Dao/Contract/LeaseDAO.h>
#include <Models/Contract/Contract.h>
#include <Dao/Engine/ModelDAOTemplate.h>

namespace DAO {

using DAO::LeaseDAO              ;
using DAO::ClientDAO             ;
using boost::mpl::int_           ;
using Models::Clients::Client    ;
using Models::Contracts::Lease   ;
using Models::Contracts::Contract;

	typedef boost::fusion::map<
		boost::fusion::pair<boost::mpl::pair<LeaseDAO , int_<0>>, Sptr<LeaseDAO >>,
		boost::fusion::pair<boost::mpl::pair<ClientDAO, int_<0>>, Sptr<ClientDAO>>
	> ContractDAODependencies;

	typedef boost::fusion::map<
		boost::fusion::pair<boost::mpl::pair<LeaseDAO , int_<0>>, function<Sptr<Lease >()>>,
		boost::fusion::pair<boost::mpl::pair<ClientDAO, int_<0>>, function<Sptr<Client>()>>
	> ContractDAOModelDependencies;

	using ContractDAO = ModelDAOTemplate<Contract, ContractDAODependencies, ContractDAOModelDependencies>;
}

#endif
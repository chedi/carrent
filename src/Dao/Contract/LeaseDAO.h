#ifndef LEASE_DAO_H
#define LEASE_DAO_H

#include <Models/Car/Car.h>
#include <Dao/Car/CarDAO.h>
#include <Dao/Client/ClientDAO.h>
#include <Models/Client/Client.h>
#include <Models/Contract/Lease.h>
#include <Dao/Engine/ModelDAOTemplate.h>

namespace DAO {	

using DAO::CarDAO             ;
using DAO::ClientDAO          ;
using boost::mpl::int_        ;
using Models::Cars::Car       ;
using Models::Clients::Client ;
using Models::Contracts::Lease;

	typedef boost::fusion::map<
		boost::fusion::pair<boost::mpl::pair<CarDAO   , int_<0>>, Sptr<CarDAO   >>,
		boost::fusion::pair<boost::mpl::pair<ClientDAO, int_<0>>, Sptr<ClientDAO>>
	> LeaseDAODependencies;

	typedef boost::fusion::map<
		boost::fusion::pair<boost::mpl::pair<CarDAO   , int_<0>>, function<Sptr<Car   >()>>,
		boost::fusion::pair<boost::mpl::pair<ClientDAO, int_<0>>, function<Sptr<Client>()>>
	> LeaseDAOModelDependencies;

	using LeaseDAO = ModelDAOTemplate<Lease, LeaseDAODependencies, LeaseDAOModelDependencies> ;
}

#endif
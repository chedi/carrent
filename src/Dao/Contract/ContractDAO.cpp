#include <Dao/Client/ClientDAO.h>
#include <Dao/Contract/LeaseDAO.h>
#include <Dao/Contract/ContractDAO.h>

namespace DAO {

using boost::fusion::at_key   ;
using boost::fusion::make_pair;

template<> const string ContractDAO::INSERT_QUERY             = "INSERT INTO contracts(client_id, lease_id) VALUES ($client_id, $lease_id) returning id;";
template<> const string ContractDAO::UPDATE_QUERY             = "UPDATE contracts SET client_id = $client_id, lease_id = $lease_id WHERE id = $id;";
template<> const string ContractDAO::DELETE_QUERY             = "DELETE FROM contracts WHERE id = $id;";
template<> const string ContractDAO::GET_ALL_QUERY            = "SELECT c.* FROM contracts c";
template<> const string ContractDAO::SERIALIZATION            = "{\"id\" : \"$id\", \"client_id\" : $client_id, \"lease_id\" : $lease_id }";
template<> const string ContractDAO::GET_BY_ID_QUERY          = "SELECT c.* FROM contracts c WHERE c.id = $id;";
template<> const string ContractDAO::GET_BY_ATTRIBUTES_QUERY  = "SELECT c.* FROM contracts c WHERE c.client_id = $client_id AND c.lease_id = $lease_id" ;
template<> const string ContractDAO::GET_TABLE_CREATION_QUERY = "CREATE TABLE contracts(id SERIAL PRIMARY KEY, client_id INT NOT NULL REFERENCES clients(id), lease_id INT NOT NULL REFERENCES leases(id))";

template<>
ContractDAO* ContractDAO::parseResult(query_row row){
	auto lease        = make_shared<LeaseDAO >(row["lease_id" ].as<int>());
	auto client       = make_shared<ClientDAO>(row["client_id"].as<int>());
	auto lease_model  = lease ->getModel();
	auto client_model = client->getModel();
	auto result = new ContractDAO(
		make_shared<Contract>(
			lease_model ,	
			client_model),
		row["id"].as<int>());
	at_key<boost::mpl::pair<LeaseDAO , int_<0>>>(result->dependencies) = lease ;
	at_key<boost::mpl::pair<ClientDAO, int_<0>>>(result->dependencies) = client;
	return result;
}

template<>
void ContractDAO::initializeFunctors() {
	serializers_map = {
		{"id"       , DAOSerializer{[this](){ return to_string(id)           ; }}},
		{"lease_id" , DAOSerializer{
			{ [this](){ return synchronizeDependency<boost::mpl::pair<LeaseDAO , int_<0>>>(false); }},
			{ [this](){ return synchronizeDependency<boost::mpl::pair<LeaseDAO , int_<0>>>(true ); }}}},
		{"client_id", DAOSerializer{
			{ [this](){ return synchronizeDependency<boost::mpl::pair<ClientDAO, int_<0>>>(false); }},
			{ [this](){ return synchronizeDependency<boost::mpl::pair<ClientDAO, int_<0>>>(true ); }}}}};

	modelDependencies = ContractDAOModelDependencies(
		make_pair<boost::mpl::pair<LeaseDAO , int_<0>>>([this](){ return model->getLease () ;}),
		make_pair<boost::mpl::pair<ClientDAO, int_<0>>>([this](){ return model->getClient() ;})
	);
	
}

}
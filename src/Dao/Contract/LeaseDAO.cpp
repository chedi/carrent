#include <Dao/Contract/LeaseDAO.h>

#include <boost/date_time/posix_time/posix_time.hpp>

namespace DAO {

using std::to_string                     ;
using boost::fusion::at_key              ;
using boost::fusion::make_pair           ;
using Models::Contracts::LeaseStatus     ;
using boost::posix_time::to_iso_string   ;
using boost::posix_time::time_from_string;

template<> const string LeaseDAO::INSERT_QUERY             = "INSERT INTO leases(car_id, client_id, status, start_date, end_date) VALUES ($car_id, $client_id, $status, '$start_date', '$end_date') returning id";
template<> const string LeaseDAO::UPDATE_QUERY             = "UPDATE leases SET car_id = $car_id, client_id = $client_id, status = $status, start_date = '$start_date', end_date = '$end_date' WHERE id = $id";
template<> const string LeaseDAO::DELETE_QUERY             = "DELETE FROM leases WHERE id = $id";
template<> const string LeaseDAO::GET_ALL_QUERY            = "SELECT l.* FROM leases l";
template<> const string LeaseDAO::SERIALIZATION            = "{ \"id\": $id, \"car\" : $car_id, \"client\" : $client_id, \"status\" : $status, \"start_date\" : \"$start_date\", \"end_date\" : \"$end_date\"}";
template<> const string LeaseDAO::GET_BY_ID_QUERY          = "SELECT l.* FROM leases l WHERE l.id = $id";
template<> const string LeaseDAO::GET_BY_ATTRIBUTES_QUERY  = "SELECT l.* FROM leases l WHERE l.car_id = $car_id AND l.client_id = $client_id AND l.status = $status AND l.start_date='$start_date' AND l.end_date='$end_date';"; 
template<> const string LeaseDAO::GET_TABLE_CREATION_QUERY = "CREATE TABLE leases(id SERIAL PRIMARY KEY, car_id INT NOT NULL REFERENCES cars(id), client_id INT NOT NULL REFERENCES clients(id), status INT NOT NULL, start_date TIMESTAMP NOT NULL, end_date TIMESTAMP NOT NULL)";

template<>
LeaseDAO* LeaseDAO::parseResult(query_row row){
	auto car          = make_shared<CarDAO   >(row["car_id"   ].as<int>());
	auto client       = make_shared<ClientDAO>(row["client_id"].as<int>());
	auto car_model    = car   ->getModel();
	auto client_model = client->getModel();
	auto result = new LeaseDAO(
		make_shared<Lease>(
			car_model                                                ,
			client_model                                             ,
			time_from_string(        row["start_date"].as<string>()) ,
			time_from_string(        row["end_date"  ].as<string>()) ,
			static_cast<LeaseStatus>(row["status"    ].as<int   >())),
		row["id"].as<int>());
	at_key<boost::mpl::pair<CarDAO , int_<0>>>(result->dependencies) = car ;
	return result;
}

template<>
void LeaseDAO::initializeFunctors() {
	serializers_map = {
		{"id"        , DAOSerializer{ [this](){ return to_string(id)                                      ; }}},
		{"status"    , DAOSerializer{ [this](){ return to_string(static_cast<int>( model->getStatus   ())); }}},
		{"end_date"  , DAOSerializer{ [this](){ return to_iso_string             (*model->getEndDate  ()) ; }}},
		{"start_date", DAOSerializer{ [this](){ return to_iso_string             (*model->getStartDate()) ; }}},
		{"car_id"    , DAOSerializer{
			{ [this](){ return synchronizeDependency<boost::mpl::pair<CarDAO   , int_<0>>>(false); }},
			{ [this](){ return synchronizeDependency<boost::mpl::pair<CarDAO   , int_<0>>>(true ); }}}},
		{"client_id"    , DAOSerializer{
			{ [this](){ return synchronizeDependency<boost::mpl::pair<ClientDAO, int_<0>>>(false); }},
			{ [this](){ return synchronizeDependency<boost::mpl::pair<ClientDAO, int_<0>>>(true ); }}}}};

	modelDependencies = LeaseDAOModelDependencies(
		make_pair<boost::mpl::pair<CarDAO   , int_<0>>>([this](){ return model->getCar   () ;}),
		make_pair<boost::mpl::pair<ClientDAO, int_<0>>>([this](){ return model->getClient() ;}));
}

}
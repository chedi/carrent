#ifndef DAO_SERIALIZER
#define DAO_SERIALIZER

#include <map>
#include <string>
#include <functional>
#include <boost/algorithm/string.hpp>


namespace DAO {

using std::map          ;
using std::string       ;
using std::function     ;
using boost::replace_all;

struct DAOSerializer{
	function<string()> getter    ;
	function<string()> serializer;

	DAOSerializer(function<string()> getter): DAOSerializer(getter, getter){}

	DAOSerializer(function<string()> getter, function<string()> serializer)
		: getter    (getter    ),
		  serializer(serializer){}
};

using SerializerMap = map<string , DAOSerializer>;

inline string fromatSerialization(string& format, SerializerMap serializers_map){
	for(auto serializer : serializers_map)
		replace_all(format,"$"+serializer.first, serializer.second.getter());
	return format;
}

}
#endif
#ifndef  CONNECTION_H
#define CONNECTION_H

#include <memory>
#include <string>
#include <pqxx/pqxx>

using namespace std;

namespace DAO {

class Connection{
	private:
		int    port;
		string host;
		string user;
		string password;
		string database;

	public:
		Connection(                                   );
		Connection(int, string, string, string, string);

		int    getPort    ();
		string getHost    ();
		string getUser    ();
		string getPassword();
		string getdatabase();

		void setPort    (int    port    );
		void setHost    (string host    );
		void setUser    (string user    );
		void setPassword(string password);
		void setdatabase(string database);

		string connectionString();
};

}
#endif
#include "Connection.h"

#include <boost/format.hpp>
#include <pqxx/except.hxx>
#include <pqxx/connection.hxx>

using boost::format;

namespace DAO{

const static string CN_STRING_TEMPLATE = "user=%2% dbname=%4% hostaddr=%3% port=%1%";

Connection::Connection(){
	this->port     = 5432;
	this->host     = "127.0.0.1";
	this->user     = "postgres";
	this->password = "";
	this->database = "carRenter";
}

Connection::Connection(int port, string host, string user, string password, string database){
	this->port     = port    ;
	this->host     = host    ;
	this->user     = user    ;
	this->password = password;
	this->database = database;
}

string Connection::connectionString(){
	string s = str(format(CN_STRING_TEMPLATE) 
		% port 
		% user      
		% host    
		% database);
	cout<<s<<endl;
	return s;
}

int    Connection::getPort    (){ return this->port     ;}
string Connection::getHost    (){ return this->host     ;}
string Connection::getUser    (){ return this->user     ;}
string Connection::getPassword(){ return this->password ;}
string Connection::getdatabase(){ return this->database ;}

void Connection::setPort    (int    port    ){ this->port     = port     ;}
void Connection::setHost    (string host    ){ this->host     = host     ;}
void Connection::setUser    (string user    ){ this->user     = user     ;}
void Connection::setPassword(string password){ this->password = password ;}
void Connection::setdatabase(string database){ this->database = database ;}

}
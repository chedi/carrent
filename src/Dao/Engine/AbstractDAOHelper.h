#ifndef ABSTRACT_DAO_HELPER_H
#define ABSTRACT_DAO_HELPER_H

#include <map>
#include <string>
#include <memory>

#include <Common/Common.h>
#include <Dao/Engine/Connection.h>
#include <Dao/Engine/DaoSerializer.h>

namespace DAO{

using std::string       ;
using std::vector       ;
using pqxx::result      ;
using std::function     ;
using DAO::DAOSerializer;

class AbstractDAOHelper {
protected:
	int                        id;
	Sptr<Connection>           connection;
	SerializerMap              serializers_map; 

	virtual void initializeFunctors() = 0;

	template<typename D>
	string synchronizeDependency(bool=false);

public:
	int          getId          ();
	bool         remove         ();
	bool         insert         ();
	bool         update         ();
	Sptr<string> serialize      ();
    int          synchronizeToDB();

	virtual const string& getAllQuery             () = 0;
	virtual const string& getByIdQuery            () = 0;
	virtual const string& getDeleteQuery          () = 0;
	virtual const string& getInsertQuery          () = 0;
	virtual const string& getUpdateQuery          () = 0;
	virtual bool          getByAttributes         () = 0;
	virtual const string& getSerialization        () = 0;
	virtual const string& getTableCreationQuery   () = 0;
	virtual const string& getFindByAttributesQuery() = 0;

	static result query(Sptr<Connection> con, string query);
};

}

#endif
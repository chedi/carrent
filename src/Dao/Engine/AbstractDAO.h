#ifndef DAO_H
#define DAO_H

#include <memory>
#include <vector>
#include <iostream>
#include <pqxx/pqxx>
#include <boost/algorithm/string.hpp>

#include <Common/Common.h>
#include <Dao/Engine/AbstractDAOHelper.h>

namespace DAO {

using pqxx::result      ;
using boost::replace_all;

template <typename Model, typename Derived>
class AbstractDAO  : public AbstractDAOHelper {
protected:
	shared_ptr<Model> model;

public:
	typedef Model modelType;

	shared_ptr<vector<shared_ptr<Derived>>> getAll         (   );
	shared_ptr<Derived>                     getById        (int);
	shared_ptr<Model>                       getModel       (   );
	bool                                    getByAttributes(   );

	virtual Derived* parseResult(pqxx::result::const_iterator) = 0;
};

template<typename Model, typename Derived> 
inline shared_ptr<Model> AbstractDAO<Model, Derived>::getModel(){
	return model;
}

template<typename Model, typename Derived> 
SVSptr<Derived> AbstractDAO<Model, Derived>::getAll(){
	SVSptr<Derived> data = SVSptr<Derived>(new VSptr<Derived>());
	result query_result = AbstractDAOHelper::query(connection, getAllQuery());

	for(auto row : query_result)
		data->push_back(shared_ptr<Derived>(parseResult(row)));
    return data;	
}

template<typename Model, typename Derived>
shared_ptr<Derived> AbstractDAO<Model, Derived>::getById(int id) {
	string query = getByIdQuery();
	for(auto serializer : serializers_map){
  		if (query.find(serializer.first) != string::npos)
			replace_all(query,"$"+serializer.first, serializer.second.getter());
	}
	result query_result = AbstractDAOHelper::query(connection, query);

	if(query_result.size() != 1)
		throw "Error getById No result found for query";

	auto row = query_result.begin();
	return shared_ptr<Derived>(parseResult(row));
}

template<typename Model, typename Derived>
bool AbstractDAO<Model, Derived>::getByAttributes() {
	try{
		string query = getFindByAttributesQuery();
		for(auto serializer : serializers_map)
			replace_all(query,"$"+serializer.first, serializer.second.getter());
		result query_result = AbstractDAOHelper::query(connection, query);

		if(query_result.size() != 1)
			return false;
		
		this->id = query_result.begin()["id"].as<int>();
		return true;
	}catch(...){
		cerr<<"Failed to find a matching item in the database"<<endl;
		return false;
}}

}

#endif
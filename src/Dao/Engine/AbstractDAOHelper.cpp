#include <boost/algorithm/string.hpp>
#include <Dao/Engine/AbstractDAOHelper.h>

namespace DAO {

using boost::replace_all;

int AbstractDAOHelper::getId() { return id ;}

pqxx::result AbstractDAOHelper::query(shared_ptr<Connection> con, string query){
	pqxx::connection cnn("user=postgres dbname=carRenter hostaddr=127.0.0.1 port=5432");
	pqxx::work txn(cnn);
	cout<<"Executing query : "<<query<<endl;
	auto result = txn.exec(query);
	txn.commit();
	return result;
}

shared_ptr<string> AbstractDAOHelper::serialize(){
	try{		
		string serialization = getSerialization();
		for(auto serializer : serializers_map)
			replace_all(serialization,"$"+serializer.first, serializer.second.serializer());
		return make_shared<string>(serialization);
	}catch(const char* e){
		cerr<<"Serialization failed with message : "<<e<<endl;
	}catch(...){
		cerr<<"Serialization failed"<<endl;
	}
	return make_shared<string>("{'status': 'Failed'}");
}

bool AbstractDAOHelper::remove() {
	try{
		string delete_query = getDeleteQuery();
		for(auto serializer : serializers_map)
			if (delete_query.find(serializer.first) != string::npos)
				replace_all(delete_query,"$"+serializer.first, serializer.second.serializer());
		pqxx::result query_result = AbstractDAOHelper::query(connection, delete_query);
		id = 0;
		return true;
	}catch(exception& e){
		cerr<<"Error while trying to remove item : "<<e.what()<<endl;
		return false;
}}

bool AbstractDAOHelper::insert(){
	try {
		string insert_query = getInsertQuery();
		for(auto serializer : serializers_map)
			replace_all(insert_query,"$"+serializer.first, serializer.second.getter());
		pqxx::result query_result = AbstractDAOHelper::query(connection, insert_query);
		this->id = query_result.begin()["id"].as<int>();
		return true;
	} catch(exception& e){
		cerr<<"Error while trying to insert item :"<<e.what()<<endl<<"Retrying recovery procedure"<<endl;
		try{
			string find_query = getFindByAttributesQuery();
			for(auto serializer : serializers_map)
				replace_all(find_query,"$"+serializer.first, serializer.second.getter());
			pqxx::result query_result = AbstractDAOHelper::query(connection, find_query);
			if(query_result.empty()){
				cerr<<"Recovery failed no item found"<<endl;
				return false;
			}
			this->id = query_result.begin()["id"].as<int>();
			cout<<"Recovery succesfull"<<endl;
			return true;
		}catch(exception& e){
			cerr<<"Error while trying to recover item :"<<e.what()<<endl;
			return false;
		}
		return false;
	} catch(...){
		return false;
}}

bool AbstractDAOHelper::update(){
	try {
		string update_query = getUpdateQuery();
		for(auto serializer : serializers_map)
			replace_all(update_query,"$"+serializer.first, serializer.second.getter());
		pqxx::result query_result = AbstractDAOHelper::query(connection, update_query);
		return true;
	} catch(exception& e){
		cerr<<"Error while trying to insert item :"<<e.what()<<endl;
		return false;
}}

int AbstractDAOHelper::synchronizeToDB(){
	try {
		getByAttributes();
		return this->id;
	}catch(...){
		if(!insert()){
			cerr<< "Failed to synchronize model with the database"<<endl;
			return 0;
		}
		return this->id;
}}

}
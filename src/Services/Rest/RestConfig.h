#ifndef REST_CONFIG_H
#define REST_CONFIG_H

#include <map>
#include <memory>
#include <vector>
#include <functional>

#include <Common/Common.h>
#include <Services/Server/Header.h>
#include <Services/Rest/Car/CarService.h>
#include <Services/Rest/Car/BrandService.h>
#include <Services/Rest/Account/UserService.h>
#include <Services/Rest/Account/UserService.h>
#include <Services/Rest/Common/PersonService.h>
#include <Services/Rest/Client/ClientService.h>
#include <Services/Rest/Contract/LeaseService.h>
#include <Services/Rest/Contract/ContractService.h>

namespace Service {

using std::string         ;
using Http::Server::header;

enum class SerializationType : unsigned char {
	XML ,
	JSON,
	YAML
};

static map<string, function<string(SVptr<string>, SVptr<header>)>> URL_ACTIONS_MAPPING{
  	{ "add_user"       , [](SVptr<string> toc, SVptr<header> headers){ return UserService    ::addUser       (toc, headers) ;}},
    { "find_user"      , [](SVptr<string> toc, SVptr<header> headers){ return UserService    ::findUser      (toc, headers) ;}},
    { "list_users"     , [](SVptr<string> toc, SVptr<header> headers){ return UserService    ::listUsers     (toc, headers) ;}},
  	{ "remove_user"    , [](SVptr<string> toc, SVptr<header> headers){ return UserService    ::removeUser    (toc, headers) ;}},
  	{ "update_user"    , [](SVptr<string> toc, SVptr<header> headers){ return UserService    ::updateUser    (toc, headers) ;}},

  	{ "add_person"     , [](SVptr<string> toc, SVptr<header> headers){ return ContractService::addContract   (toc, headers) ;}},
    { "find_person"    , [](SVptr<string> toc, SVptr<header> headers){ return ContractService::findContract  (toc, headers) ;}},
    { "list_persons"   , [](SVptr<string> toc, SVptr<header> headers){ return ContractService::listContracts (toc, headers) ;}},
  	{ "remove_person"  , [](SVptr<string> toc, SVptr<header> headers){ return ContractService::removeContract(toc, headers) ;}},
  	{ "update_person"  , [](SVptr<string> toc, SVptr<header> headers){ return ContractService::updateContract(toc, headers) ;}},

    { "add_brand"      , [](SVptr<string> toc, SVptr<header> headers){ return BrandService   ::addBrand      (toc, headers) ;}},
    { "find_brand"     , [](SVptr<string> toc, SVptr<header> headers){ return BrandService   ::findBrand     (toc, headers) ;}},
    { "list_brands"    , [](SVptr<string> toc, SVptr<header> headers){ return BrandService   ::listBrands    (toc, headers) ;}},
  	{ "remove_brand"   , [](SVptr<string> toc, SVptr<header> headers){ return BrandService   ::removeBrand   (toc, headers) ;}},
  	{ "update_brand"   , [](SVptr<string> toc, SVptr<header> headers){ return BrandService   ::updateBrand   (toc, headers) ;}},

  	{ "add_car"        , [](SVptr<string> toc, SVptr<header> headers){ return CarService     ::addCar        (toc, headers) ;}},
    { "find_car"       , [](SVptr<string> toc, SVptr<header> headers){ return CarService     ::findCar       (toc, headers) ;}},
    { "list_cars"      , [](SVptr<string> toc, SVptr<header> headers){ return CarService     ::listCars      (toc, headers) ;}},
  	{ "remove_car"     , [](SVptr<string> toc, SVptr<header> headers){ return CarService     ::removeCar     (toc, headers) ;}},
  	{ "update_car"     , [](SVptr<string> toc, SVptr<header> headers){ return CarService     ::updateCar     (toc, headers) ;}},

  	{ "add_client"     , [](SVptr<string> toc, SVptr<header> headers){ return ClientService  ::addClient     (toc, headers) ;}},
    { "find_client"    , [](SVptr<string> toc, SVptr<header> headers){ return ClientService  ::findClient    (toc, headers) ;}},
    { "list_clients"   , [](SVptr<string> toc, SVptr<header> headers){ return ClientService  ::listClients   (toc, headers) ;}},
  	{ "remove_client"  , [](SVptr<string> toc, SVptr<header> headers){ return ClientService  ::removeClient  (toc, headers) ;}},
  	{ "update_client"  , [](SVptr<string> toc, SVptr<header> headers){ return ClientService  ::updateClient  (toc, headers) ;}},

  	{ "add_lease"      , [](SVptr<string> toc, SVptr<header> headers){ return LeaseService   ::addLease      (toc, headers) ;}},
    { "find_lease"     , [](SVptr<string> toc, SVptr<header> headers){ return LeaseService   ::findLease     (toc, headers) ;}},
    { "list_leases"    , [](SVptr<string> toc, SVptr<header> headers){ return LeaseService   ::listLeases    (toc, headers) ;}},
  	{ "remove_lease"   , [](SVptr<string> toc, SVptr<header> headers){ return LeaseService   ::removeLease   (toc, headers) ;}},
  	{ "update_lease"   , [](SVptr<string> toc, SVptr<header> headers){ return LeaseService   ::updateLease   (toc, headers) ;}},

};

}

#endif
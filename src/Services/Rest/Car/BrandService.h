#ifndef BRAND_SERVICE_H
#define BRAND_SERVICE_H

#include <vector>
#include <string>

#include <Common/Common.h>
#include <Dao/Car/BrandDAO.h>
#include <Services/Server/Header.h>

namespace Service {

using std::string         ;
using std::vector         ;
using DAO::BrandDAO       ;
using Http::Server::header;

class BrandService {
public:

	static string getBrand   (SVptr<string>, SVptr<header>);
	static string addBrand   (SVptr<string>, SVptr<header>);
	static string findBrand  (SVptr<string>, SVptr<header>);
	static string listBrands (SVptr<string>, SVptr<header>);
	static string removeBrand(SVptr<string>, SVptr<header>);
	static string updateBrand(SVptr<string>, SVptr<header>);
};

}

#endif
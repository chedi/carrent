#include <iostream>
#include <Dao/Car/CarDAO.h>
#include <Services/Rest/Engine/Utils.h>
#include <Services/Rest/Car/BrandService.h>
#include <Services/Rest/Engine/HttpHeaderParser.h>

namespace Service {

using std::move              ;
using std::stoi              ;
using std::cout              ;
using std::endl              ;
using DAO::BrandDAO          ;
using Rest::findModel        ;
using boost::any_cast        ;
using Rest::removeModel      ;
using Models::Cars::Brand    ;
using Rest::SUCCES_STATUS    ;
using boost::fusion::at_key  ;
using Rest::HttpHeaderParser ;

string BrandService::listBrands(SVptr<string> rest_tokens, SVptr<header> headers){
	BrandDAO tmp;
	auto brands = tmp.getAll();
	string result = "{\"Brands\": [";
	for(auto brand : *brands)
		result += *brand->serialize();
	result += "]}";
	return result;
}

string BrandService::addBrand(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		string model        = any_cast<string>(params.at("model"        ));
		string manufacturer = any_cast<string>(params.at("manufacturer" ));
		auto result = new BrandDAO(make_shared<Brand>(move(model), move(manufacturer)));

		result->insert();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

string BrandService::findBrand(SVptr<string> rest_tokens, SVptr<header> headers){
	return findModel<BrandDAO>(rest_tokens, headers);
}

string BrandService::removeBrand(SVptr<string> rest_tokens, SVptr<header> headers){
	return removeModel<BrandDAO>(rest_tokens, headers);
}

string BrandService::updateBrand(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		int    id           = any_cast<int   >(params.at("id"        ));
		string model        = any_cast<string>(params.at("model"        ));
		string manufacturer = any_cast<string>(params.at("manufacturer" ));
		
		BrandDAO brand(id);

		brand.getModel()->setModel       (make_shared<string>(model       ));
		brand.getModel()->setManufacturer(make_shared<string>(manufacturer));

		brand.update();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

}

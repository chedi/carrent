#include <iostream>
#include <Dao/Car/CarDAO.h>
#include <Services/Rest/Engine/Utils.h>
#include <Services/Rest/Car/CarService.h>
#include <Services/Rest/Engine/HttpHeaderParser.h>

namespace Service {

using std::move             ;
using std::stoi             ;
using std::cout             ;
using std::endl             ;
using DAO::CarDAO           ;
using DAO::BrandDAO         ;
using Rest::findModel       ;
using boost::any_cast       ;
using Models::Cars::Car     ;
using Rest::removeModel     ;
using Rest::SUCCES_STATUS   ;
using boost::fusion::at_key ;
using Rest::HttpHeaderParser;

string CarService::listCars(SVptr<string> rest_tokens, SVptr<header> headers){
	CarDAO tmp;
	auto cars = tmp.getAll();
	string result = "{\"Cars\": [";
	for(auto car : *cars)
		result += *car->serialize();
	result += "]}";
	return result;
}

string CarService::addCar(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		string serial    = any_cast<string>(params.at("serial"  ));
		int    brand_id  = any_cast<int   >(params.at("brand_id"));
		auto brand       = make_shared<BrandDAO>(brand_id);
		auto brand_model = brand->getModel();
		auto result = new CarDAO(make_shared<Car>(brand_model, move(serial)));

		result->insert();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

string CarService::findCar(SVptr<string> rest_tokens, SVptr<header> headers){
	return findModel<CarDAO>(rest_tokens, headers);
}

string CarService::removeCar(SVptr<string> rest_tokens, SVptr<header> headers){
	return removeModel<CarDAO>(rest_tokens, headers);
}

string CarService::updateCar(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		int    id       = any_cast<int   >(params.at("id"       ));
		string serial   = any_cast<string>(params.at("serial"  ));
		int    brand_id = any_cast<int   >(params.at("brand_id"));
		
		CarDAO   car  (id      );
		BrandDAO brand(brand_id);

		car.getModel()->setSerial(make_shared<string>(serial          ));
		car.getModel()->setBrand (make_shared<Brand >(brand.getModel()));

		car.update();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

}

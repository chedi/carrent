#ifndef CAR_SERVICE_H
#define CAR_SERVICE_H

#include <vector>
#include <string>

#include <Common/Common.h>
#include <Dao/Car/CarDAO.h>
#include <Services/Server/Header.h>

namespace Service {

using std::string         ;
using std::vector         ;
using DAO::CarDAO         ;
using Http::Server::header;

class CarService {
public:

	static string getCar   (SVptr<string>, SVptr<header>);
	static string addCar   (SVptr<string>, SVptr<header>);
	static string findCar  (SVptr<string>, SVptr<header>);
	static string listCars (SVptr<string>, SVptr<header>);
	static string removeCar(SVptr<string>, SVptr<header>);
	static string updateCar(SVptr<string>, SVptr<header>);
};

}

#endif
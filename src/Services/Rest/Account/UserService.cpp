#include <iostream>
#include <Dao/Account/UserDAO.h>
#include <Services/Rest/Engine/Utils.h>
#include <Services/Rest/Account/UserService.h>
#include <Services/Rest/Engine/HttpHeaderParser.h>

namespace Service {

using std::move             ;
using std::stoi             ;
using std::cout             ;
using std::endl             ;
using DAO::UserDAO          ;
using DAO::PersonDAO        ;
using Rest::findModel       ;
using boost::any_cast       ;
using Rest::removeModel     ;
using Rest::SUCCES_STATUS   ;
using boost::fusion::at_key ;
using Rest::HttpHeaderParser;
using Models::Accounts::User;

string UserService::listUsers(SVptr<string> rest_tokens, SVptr<header> headers){
	UserDAO tmp;
	auto users = tmp.getAll();
	string result = "{\"Users\": [";
	for(auto user : *users)
		result += *user->serialize();
	result += "]}";
	return result;
}

string UserService::addUser(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		string mail      = any_cast<string>(params.at("mail"     ));
		string login     = any_cast<string>(params.at("login"    ));
		bool   status    = any_cast<bool  >(params.at("status"   ));
		string password  = any_cast<string>(params.at("password" ));
		int    person_id = any_cast<int   >(params.at("person_id"));
		
		auto person       = make_shared<PersonDAO>(person_id);
		auto person_model = person->getModel();
		auto result = new UserDAO(make_shared<User>(person_model, move(mail), move(login), move(password), status));

		result->insert();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

string UserService::findUser(SVptr<string> rest_tokens, SVptr<header> headers){
	return findModel<UserDAO>(rest_tokens, headers);
}

string UserService::removeUser(SVptr<string> rest_tokens, SVptr<header> headers){
	return removeModel<UserDAO>(rest_tokens, headers);
}

string UserService::updateUser(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		int    id        = any_cast<int   >(params.at("id"       ));
		string mail      = any_cast<string>(params.at("mail"     ));
		string login     = any_cast<string>(params.at("login"    ));
		bool   status    = any_cast<bool  >(params.at("status"   ));
		string password  = any_cast<string>(params.at("password" ));
		int    person_id = any_cast<int   >(params.at("person_id"));
		
		UserDAO   user  (id       );
		PersonDAO person(person_id);

		user.getModel()->setMail    (make_shared<string>(mail    ));
		user.getModel()->setLogin   (make_shared<string>(login   ));
		user.getModel()->setStatus  (status                       );
		user.getModel()->setPerson  (make_shared<Person>(person.getModel()  ));
		user.getModel()->setPassword(make_shared<string>(password));

		user.update();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

}

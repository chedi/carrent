#ifndef USER_SERVICE_H
#define USER_SERVICE_H

#include <vector>
#include <string>

#include <Common/Common.h>
#include <Dao/Account/UserDAO.h>
#include <Services/Server/Header.h>

namespace Service {

using std::string         ;
using std::vector         ;
using DAO::UserDAO        ;
using Http::Server::header;

class UserService {
public:

	static string getUser   (SVptr<string>, SVptr<header>);
	static string addUser   (SVptr<string>, SVptr<header>);
	static string findUser  (SVptr<string>, SVptr<header>);
	static string listUsers (SVptr<string>, SVptr<header>);
	static string removeUser(SVptr<string>, SVptr<header>);
	static string updateUser(SVptr<string>, SVptr<header>);
};

}

#endif
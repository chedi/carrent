#include <iostream>
#include <Dao/Common/PersonDAO.h>
#include <Services/Rest/Engine/Utils.h>
#include <Services/Rest/Common/PersonService.h>
#include <Services/Rest/Engine/HttpHeaderParser.h>

namespace Service {

using std::move              ;
using std::stoi              ;
using std::cout              ;
using std::endl              ;
using DAO::PersonDAO         ;
using Rest::findModel        ;
using boost::any_cast        ;
using Rest::removeModel      ;
using Rest::SUCCES_STATUS    ;
using boost::fusion::at_key  ;
using Rest::HttpHeaderParser ;
using Models::Commons::Person;

string PersonService::listPersons(SVptr<string> rest_tokens, SVptr<header> headers){
	PersonDAO tmp;
	auto persons = tmp.getAll();
	string result = "{\"Persons\": [";
	for(auto person : *persons)
		result += *person->serialize();
	result += "]}";
	return result;
}

string PersonService::addPerson(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		string nid    = any_cast<string>(params.at("nid"       ));
		string l_name = any_cast<string>(params.at("last_name" ));
		string f_name = any_cast<string>(params.at("first_name"));
		
		auto result = new PersonDAO(make_shared<Person>(move(nid), move(f_name), move(l_name)));

		result->insert();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

string PersonService::findPerson(SVptr<string> rest_tokens, SVptr<header> headers){
	return findModel<PersonDAO>(rest_tokens, headers);
}

string PersonService::removePerson(SVptr<string> rest_tokens, SVptr<header> headers){
	return removeModel<PersonDAO>(rest_tokens, headers);
}

string PersonService::updatePerson(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		int    id     = any_cast<int   >(params.at("id"        ));
		string nid    = any_cast<string>(params.at("nid"       ));
		string l_name = any_cast<string>(params.at("last_name" ));
		string f_name = any_cast<string>(params.at("first_name"));
		
		PersonDAO person(id);

		person.getModel()->setNid      (make_shared<string>(nid   ));
		person.getModel()->setLastName (make_shared<string>(l_name));
		person.getModel()->setFirstName(make_shared<string>(f_name));

		person.update();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

}

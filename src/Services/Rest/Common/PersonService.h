#ifndef PERSON_SERVICE_H
#define PERSON_SERVICE_H

#include <vector>
#include <string>

#include <Common/Common.h>
#include <Dao/Common/PersonDAO.h>
#include <Services/Server/Header.h>

namespace Service {

using std::string         ;
using std::vector         ;
using DAO::PersonDAO      ;
using Http::Server::header;

class PersonService {
public:

	static string getPerson   (SVptr<string>, SVptr<header>);
	static string addPerson   (SVptr<string>, SVptr<header>);
	static string findPerson  (SVptr<string>, SVptr<header>);
	static string listPersons (SVptr<string>, SVptr<header>);
	static string removePerson(SVptr<string>, SVptr<header>);
	static string updatePerson(SVptr<string>, SVptr<header>);
};

}

#endif
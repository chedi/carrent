#include <map>
#include <string>
#include <iostream>
#include <stdexcept>
#include <functional>
#include <boost/mpl/map.hpp>
#include <boost/mpl/has_key.hpp>
#include <boost/fusion/include/pair.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <Services/Rest/Engine/HttpHeaderParser.h>

namespace Rest {

using std::invalid_argument ;
using std::out_of_range;

using std::map                          ;
using std::cout                         ;
using std::cerr                         ;
using std::endl                         ;
using std::string                       ;
using boost::regex                      ;
using std::function                     ;
using boost::cmatch                     ;
using boost::regex_match                ;
using boost::posix_time::ptime          ;
using boost::posix_time::to_iso_string  ;
using boost::posix_time::from_iso_string;

struct BaseParam {};


const regex HttpHeaderParser::header_name_regex("rest_param_([a-zA-Z_]+)_([a-zA-Z_]+)");

static map<string, function<boost::any(string)>> TYPES_CONVERTER{
	{"int"             , [](string data){ return boost::any(std::stoi  (data)); }},
	{"long"            , [](string data){ return boost::any(std::stol  (data)); }},
	{"float"           , [](string data){ return boost::any(std::stof  (data)); }},
	{"double"          , [](string data){ return boost::any(std::stod  (data)); }},
	{"longlong"        , [](string data){ return boost::any(std::stoll (data)); }},
	{"longdouble"      , [](string data){ return boost::any(std::stold (data)); }},
	{"unsignedlong"    , [](string data){ return boost::any(std::stoul (data)); }},
	{"unsignedlonglong", [](string data){ return boost::any(std::stoull(data)); }},

	{"bool"            , [](string data){ return boost::any(data == "true" ? true : false); }},
	{"ptime"           , [](string data){ return boost::any(from_iso_string(data))        ; }},
	{"string"          , [](string data){ return boost::any(data)                         ; }},
};

HttpHeaderParser::HttpHeaderParser(SVptr<header> headers){
	for(auto header : *headers){
		cmatch groups;
		if(regex_match(header.name.c_str(), groups, header_name_regex)){
			auto param_name = groups[1];
			auto param_type = groups[2];
			try{
				auto convert_func = TYPES_CONVERTER.at(param_type);
				auto param_value  = convert_func(header.value);
				parameters[param_name] = param_value;
			}catch(out_of_range& e){
				cerr<<"Malformed header parameter name : "<<header.name<<endl<<e.what()<<endl;
			}catch(invalid_argument& e){
				cerr<<"Unable to convert header value to requested type : "<<e.what()<<endl;
			}
	}}
}

const map<string, any> HttpHeaderParser::getParameters() { return parameters; }

}
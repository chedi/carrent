#ifndef HTTP_HEADER_PARSER_H
#define HTTP_HEADER_PARSER_H

#include <map>
#include <vector>
#include <string>
#include <boost/any.hpp>
#include <boost/regex.hpp>

#include <Common/Common.h>
#include <Services/Server/Header.h>

namespace Rest {

using std::map            ;
using boost::any          ;
using std::vector         ;
using std::string         ;
using boost::regex        ;
using boost::any_cast     ;
using Http::Server::header;

class HttpHeaderParser{
	map<string, any>   parameters       ;
	static const regex header_name_regex;

public:
	HttpHeaderParser(                        ) = delete;
	HttpHeaderParser(const HttpHeaderParser& ) = delete;
	HttpHeaderParser(      HttpHeaderParser&&) = delete;

	HttpHeaderParser(SVptr<header>);

	const map<string, any> getParameters();
};

}

#endif
#ifndef UTILS_H
#define UTILS_H

#include <Common/Common.h>
#include <Services/Server/Header.h>

namespace Rest{

using Http::Server::header;

const string SUCCES_STATUS = "{\"Status\" : \"success\"}";

template <typename T>
string findModel(SVptr<string> rest_tokens, SVptr<header> headers){
	try{
		if(rest_tokens->size() != 1)
			throw "Non conformant request";
		T tmp(stoi(rest_tokens->at(0)));
		return *tmp.serialize();
	}catch(std::invalid_argument& e){
		throw "failed to convert user id";
	}catch(...){
		throw "failed to find user";
	}
}

template <typename T>
string removeModel(SVptr<string> rest_tokens, SVptr<header> headers){
	try{
		if(rest_tokens->size() != 1)
			throw "Non conformant request";
		T tmp(stoi(rest_tokens->at(0)));
		tmp.remove();
		return SUCCES_STATUS;
	}catch(std::invalid_argument& e){
		throw "failed to convert user id";
	}catch(...){
		throw "failed to remove user";
	}
}

}
#endif
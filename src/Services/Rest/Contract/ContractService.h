#ifndef CONTRACT_SERVICE_H
#define CONTRACT_SERVICE_H

#include <vector>
#include <string>

#include <Common/Common.h>
#include <Services/Server/Header.h>
#include <Dao/Contract/ContractDAO.h>

namespace Service {

using std::string         ;
using std::vector         ;
using DAO::ContractDAO    ;
using Http::Server::header;

class ContractService {
public:

	static string getContract   (SVptr<string>, SVptr<header>);
	static string addContract   (SVptr<string>, SVptr<header>);
	static string findContract  (SVptr<string>, SVptr<header>);
	static string listContracts (SVptr<string>, SVptr<header>);
	static string removeContract(SVptr<string>, SVptr<header>);
	static string updateContract(SVptr<string>, SVptr<header>);
};

}

#endif
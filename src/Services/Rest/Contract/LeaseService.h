#ifndef LEASE_SERVICE_H
#define LEASE_SERVICE_H

#include <vector>
#include <string>

#include <Common/Common.h>
#include <Dao/Contract/LeaseDAO.h>
#include <Services/Server/Header.h>

namespace Service {

using std::string         ;
using std::vector         ;
using DAO::LeaseDAO       ;
using Http::Server::header;

class LeaseService {
public:

	static string getLease   (SVptr<string>, SVptr<header>);
	static string addLease   (SVptr<string>, SVptr<header>);
	static string findLease  (SVptr<string>, SVptr<header>);
	static string listLeases (SVptr<string>, SVptr<header>);
	static string removeLease(SVptr<string>, SVptr<header>);
	static string updateLease(SVptr<string>, SVptr<header>);
};

}

#endif
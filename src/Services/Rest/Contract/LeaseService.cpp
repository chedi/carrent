#include <iostream>
#include <Dao/Contract/LeaseDAO.h>
#include <Services/Rest/Engine/Utils.h>
#include <Services/Rest/Contract/LeaseService.h>
#include <Services/Rest/Engine/HttpHeaderParser.h>

namespace Service {

using std::move                     ;
using std::stoi                     ;
using std::cout                     ;
using std::endl                     ;
using DAO::CarDAO                   ;
using DAO::LeaseDAO                 ;
using DAO::ClientDAO                ;
using Rest::findModel               ;
using boost::any_cast               ;
using Models::Cars::Car             ;
using Rest::removeModel             ;
using Rest::SUCCES_STATUS           ;
using boost::fusion::at_key         ;
using Rest::HttpHeaderParser        ;
using Models::Contracts::Lease      ;
using boost::posix_time::ptime      ;
using Models::Contracts::LeaseStatus;

string LeaseService::listLeases(SVptr<string> rest_tokens, SVptr<header> headers){
	LeaseDAO tmp;
	auto leases = tmp.getAll();
	string result = "{\"Leases\": [";
	for(auto lease : *leases)
		result += *lease->serialize();
	result += "]}";
	return result;
}

string LeaseService::addLease(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		ptime       end_date   =                          any_cast<ptime>(params.at("end_date"   ));
		ptime       start_date =                          any_cast<ptime>(params.at("start_date" ));
		LeaseStatus status     = static_cast<LeaseStatus>(any_cast<int  >(params.at("status"    )));
		int         car_id     =                          any_cast<int  >(params.at("car_id"     ));
		int         client_id  =                          any_cast<int  >(params.at("client_id"  ));
		
		auto car          = make_shared<CarDAO   >(car_id   );
		auto client       = make_shared<ClientDAO>(client_id);
		auto car_model    = car   ->getModel();
		auto client_model = client->getModel();
		
		auto result = new LeaseDAO(make_shared<Lease>(car_model, client_model, move(start_date), move(end_date), status));

		result->insert();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

string LeaseService::findLease(SVptr<string> rest_tokens, SVptr<header> headers){
	return findModel<LeaseDAO>(rest_tokens, headers);
}

string LeaseService::removeLease(SVptr<string> rest_tokens, SVptr<header> headers){
	return removeModel<LeaseDAO>(rest_tokens, headers);
}

string LeaseService::updateLease(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		int         id         =                          any_cast<int  >(params.at("id"         ));
		ptime       end_date   =                          any_cast<ptime>(params.at("end_date"   ));
		ptime       start_date =                          any_cast<ptime>(params.at("start_date" ));
		LeaseStatus status     = static_cast<LeaseStatus>(any_cast<int  >(params.at("status"    )));
		int         car_id     =                          any_cast<int  >(params.at("car_id"     ));
		int         client_id  =                          any_cast<int  >(params.at("client_id"  ));
		
		LeaseDAO  lease (id       );
		CarDAO    car   (car_id   );
		ClientDAO client(client_id);

		lease.getModel()->setEndDate  (make_shared<ptime>(end_date          ));
		lease.getModel()->setStartDate(make_shared<ptime>(start_date        ));
		lease.getModel()->setStatus   (status                                );
		lease.getModel()->setCar      (make_shared<Car   >(car   .getModel()));
		lease.getModel()->setClient   (make_shared<Client>(client.getModel()));

		lease.update();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

}

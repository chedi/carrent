#include <iostream>
#include <Dao/Contract/ContractDAO.h>
#include <Services/Rest/Engine/Utils.h>
#include <Services/Rest/Contract/ContractService.h>
#include <Services/Rest/Engine/HttpHeaderParser.h>

namespace Service {

using std::move                     ;
using std::stoi                     ;
using std::cout                     ;
using std::endl                     ;
using DAO::LeaseDAO                 ;
using DAO::ClientDAO                ;
using Rest::findModel               ;
using boost::any_cast               ;
using DAO::ContractDAO              ;
using Rest::removeModel             ;
using Rest::SUCCES_STATUS           ;
using boost::fusion::at_key         ;
using Rest::HttpHeaderParser        ;
using Models::Contracts::Lease      ;
using Models::Contracts::Contract   ;
using Models::Contracts::LeaseStatus;

string ContractService::listContracts(SVptr<string> rest_tokens, SVptr<header> headers){
	ContractDAO tmp;
	auto contracts = tmp.getAll();
	string result = "{\"Contracts\": [";
	for(auto contract : *contracts)
		result += *contract->serialize();
	result += "]}";
	return result;
}

string ContractService::addContract(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		int lease_id  = any_cast<int  >(params.at("lease_id" ));
		int client_id = any_cast<int  >(params.at("client_id"));
		
		auto lease    = make_shared<LeaseDAO >(lease_id );
		auto client   = make_shared<ClientDAO>(client_id);

		auto lease_model  = lease ->getModel();
		auto client_model = client->getModel();
		auto result = new ContractDAO(make_shared<Contract>(lease_model, client_model));

		result->insert();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

string ContractService::findContract(SVptr<string> rest_tokens, SVptr<header> headers){
	return findModel<ContractDAO>(rest_tokens, headers);
}

string ContractService::removeContract(SVptr<string> rest_tokens, SVptr<header> headers){
	return removeModel<ContractDAO>(rest_tokens, headers);
}

string ContractService::updateContract(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		int id        = any_cast<int  >(params.at("id"       ));
		int lease_id  = any_cast<int  >(params.at("lease_id" ));
		int client_id = any_cast<int  >(params.at("client_id"));

		LeaseDAO     lease   (lease_id );
		ClientDAO    client  (client_id);
		ContractDAO  contract(id       );

		contract.getModel()->setLease (make_shared<Lease >(lease .getModel()));
		contract.getModel()->setClient(make_shared<Client>(client.getModel()));

		contract.update();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

}

#ifndef CLIENT_SERVICE_H
#define CLIENT_SERVICE_H

#include <vector>
#include <string>

#include <Common/Common.h>
#include <Dao/Client/ClientDAO.h>
#include <Services/Server/Header.h>

namespace Service {

using std::string         ;
using std::vector         ;
using DAO::ClientDAO      ;
using Http::Server::header;

class ClientService {
public:

	static string getClient   (SVptr<string>, SVptr<header>);
	static string addClient   (SVptr<string>, SVptr<header>);
	static string findClient  (SVptr<string>, SVptr<header>);
	static string listClients (SVptr<string>, SVptr<header>);
	static string removeClient(SVptr<string>, SVptr<header>);
	static string updateClient(SVptr<string>, SVptr<header>);
};

}

#endif
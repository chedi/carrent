#include <iostream>
#include <Dao/Client/ClientDAO.h>
#include <Services/Rest/Engine/Utils.h>
#include <Services/Rest/Client/ClientService.h>
#include <Services/Rest/Engine/HttpHeaderParser.h>

namespace Service {

using std::move              ;
using std::stoi              ;
using std::cout              ;
using std::endl              ;
using DAO::ClientDAO         ;
using DAO::PersonDAO         ;
using Rest::findModel        ;
using boost::any_cast        ;
using Rest::removeModel      ;
using Rest::SUCCES_STATUS    ;
using boost::fusion::at_key  ;
using Rest::HttpHeaderParser ;
using Models::Clients::Client;

string ClientService::listClients(SVptr<string> rest_tokens, SVptr<header> headers){
	ClientDAO tmp;
	auto clients = tmp.getAll();
	string result = "{\"Clients\": [";
	for(auto client : *clients)
		result += *client->serialize();
	result += "]}";
	return result;
}

string ClientService::addClient(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		int    age        = any_cast<int   >(params.at("age"           ));
		string d_licence  = any_cast<string>(params.at("driver_licence"));
		int    person_id  = any_cast<int   >(params.at("person_id"     ));

		auto person       = make_shared<PersonDAO>(person_id);
		auto person_model = person->getModel();
		auto result       = new ClientDAO(make_shared<Client>(person_model, move(d_licence), age));

		result->insert();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

string ClientService::findClient(SVptr<string> rest_tokens, SVptr<header> headers){
	return findModel<ClientDAO>(rest_tokens, headers);
}

string ClientService::removeClient(SVptr<string> rest_tokens, SVptr<header> headers){
	return removeModel<ClientDAO>(rest_tokens, headers);
}

string ClientService::updateClient(SVptr<string> rest_tokens, SVptr<header> headers){
	auto params = HttpHeaderParser(headers).getParameters();
	try{
		int    id         = any_cast<int   >(params.at("id"            ));
		int    age        = any_cast<int   >(params.at("age"           ));
		string d_licence  = any_cast<string>(params.at("driver_licence"));
		int    person_id  = any_cast<int   >(params.at("person_id"     ));
		
		ClientDAO client(id       );
		PersonDAO person(person_id);

		client.getModel()->setAge          (age                                   );
		client.getModel()->setPerson       (make_shared<Person>(person.getModel()));
		client.getModel()->setDriverLicence(make_shared<string>(d_licence        ));

		client.update();
		return SUCCES_STATUS;
	}catch(...){
		throw "failed to parse headers params";
	}
}

}

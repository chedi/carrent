#ifndef PERSON_H
#define PERSON_H

#include <string>
#include <memory>

#include <Common/Common.h>

namespace Models  {
namespace Commons {

class Person{
		Sptr<string> nid       ;
		Sptr<string> last_name ;
		Sptr<string> first_name;

	public:
		Person(      Person &&                            ) = default;
		Person(const Person &                             ) = default;
		Person(Sptr <Person>                              );

		Person(     string& ,      string& ,      string& );
		Person(     string&&,      string&&,      string&&);
		Person(Sptr<string> , Sptr<string> , Sptr<string> );

		Sptr<string> getNid      ();
		Sptr<string> getLastName ();
		Sptr<string> getFirstName();

		void setNid      (Sptr<string>);
		void setLastName (Sptr<string>);
		void setFirstName(Sptr<string>);
};

}}
#endif

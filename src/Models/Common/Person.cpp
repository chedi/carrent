#include <Models/Common/Person.h>

namespace Models  {
namespace Commons {

Person::Person(Sptr<Person> person) 
	: Person( 
		person->nid       ,
		person->first_name,
		person->last_name ){}

Person::Person(string& nid, string& first_name, string& last_name)
	: Person(
		make_shared<string> (move(nid       )),
	    make_shared<string> (move(first_name)),
	    make_shared<string> (move(last_name ))){}

Person::Person(string&& nid, string&& first_name, string&& last_name)
	: Person(
		make_shared<string> (nid       ),
	    make_shared<string> (first_name),
	    make_shared<string> (last_name )){}

Person::Person(Sptr<string> nid, Sptr<string> first_name, Sptr<string> last_name)
	:   nid       (nid       ),
	    last_name (last_name ),
	    first_name(first_name){}

Sptr<string> Person::getNid      (){ return nid       ;}
Sptr<string> Person::getLastName (){ return last_name ;}
Sptr<string> Person::getFirstName(){ return first_name;}

void Person::setNid      (Sptr<string> nid       ) { this->nid        = nid       ;}
void Person::setLastName (Sptr<string> last_name ) { this->last_name  = last_name ;}
void Person::setFirstName(Sptr<string> first_name) { this->first_name = first_name;}

}}
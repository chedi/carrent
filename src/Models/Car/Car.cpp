#include "Car.h"

namespace Models {
namespace Cars   {

Car::Car(Sptr<Car> car)
	: Car(
		car->brand,
		car->serial){}

Car::Car(Sptr<Brand> brand, string&& serial)
	: Car(
		brand                      ,
		make_shared<string>(serial)){}

Car::Car(Sptr<Brand> brand, Sptr<string> serial) 
	:   brand (brand ),
	    serial(serial){}

Sptr<Brand > Car::getBrand () { return brand  ;}
Sptr<string> Car::getSerial() { return serial ;}

void Car::setBrand (Sptr<Brand>  brand ) { this->brand  = brand ;}
void Car::setSerial(Sptr<string> serial) { this->serial = serial;}

}}
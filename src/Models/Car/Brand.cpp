#include "Brand.h"

namespace Models {
namespace Cars   {

Brand::Brand(Sptr<Brand> brand)
	: Brand(
		brand->model,
		brand->manufacturer){}

Brand::Brand(string&& model, string&& manufacturer)
	: Brand(
		make_shared<string>(model        ),
		make_shared<string>(manufacturer)){}

Brand::Brand(Sptr<string> model, Sptr<string> manufacturer) 
	:   model(model),
	    manufacturer(manufacturer){}

Sptr<string> Brand::getModel       (){ return model        ;}
Sptr<string> Brand::getManufacturer(){ return manufacturer ;}

void Brand::setModel       (Sptr<string> model       ){ this->model        = model        ;}
void Brand::setManufacturer(Sptr<string> manufacturer){ this->manufacturer = manufacturer ;}


}}

#ifndef CAR_H_
#define CAR_H_

#include <memory>
#include <string>

#include <Common/Common.h>
#include <Models/Car/Brand.h>

using Models::Cars::Brand;

namespace Models {
namespace Cars   {

class Car {
		Sptr<Brand > brand ;
		Sptr<string> serial;

	public:
		Car(const Car&                ) = default;
		Car(      Car&&               ) = default;
		Car(Sptr <Car>                );

		Car(Sptr<Brand>,      string&&);
		Car(Sptr<Brand>, Sptr<string> );

		Sptr<Brand > getBrand ();
		Sptr<string> getSerial();

		void setBrand (Sptr<Brand> );
		void setSerial(Sptr<string>);
};

}}
#endif

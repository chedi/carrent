#ifndef BRAND_H_
#define BRAND_H_

#include <string>
#include <memory>

#include <Common/Common.h>

namespace Models {
namespace Cars   {

class Brand {
		Sptr<string> model;
		Sptr<string> manufacturer;

	public:
		Brand(      Brand&&               ) = default;
		Brand(const Brand&                ) = default;
		Brand(Sptr <Brand>                );
		
		Brand(     string&&,      string&&);
		Brand(Sptr<string> , Sptr<string> );

		Sptr<string> getModel       ();
		Sptr<string> getManufacturer();

		void setModel       (Sptr<string>);
		void setManufacturer(Sptr<string>);
};

}}
#endif

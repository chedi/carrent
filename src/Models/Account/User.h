#ifndef MODEL_USER_H
#define MODEL_USER_H

#include <memory>
#include <string>

#include <Common/Common.h>
#include <Models/Common/Person.h>

using namespace Models::Commons;

namespace Models   {
namespace Accounts {

class User {
		bool status;

		Sptr<string> mail    ;
		Sptr<string> login   ;
		Sptr<Person> person  ;
		Sptr<string> password;

	public:
		User(      User &&                                                       ) = default;
		User(const User &                                                        ) = default;
		User(Sptr <User>                                                         );
	
		User(Sptr<Person>,      string&&,      string&&,      string&&, bool=true);
		User(Sptr<Person>, Sptr<string> , Sptr<string> , Sptr<string> , bool=true);

		Sptr<string> getMail    ();
		Sptr<string> getLogin   ();
		bool         getStatus  ();
		Sptr<Person> getPerson  ();
		Sptr<string> getPassword();

		void setMail    (Sptr<string>);
		void setLogin   (Sptr<string>);
		void setStatus  (bool        );
		void setPerson  (Sptr<Person>);
		void setPassword(Sptr<string>);
};

}}
#endif
#include "User.h"

namespace Models  {
namespace Accounts{

User::User(Sptr<User> user)
	: User(
		user->person  , 
		user->mail    , 
		user->login   , 
		user->password, 
		user->status  ){}

User::User(Sptr<Person> person, string&& mail, string&& login, string&& password, bool status)
	: User(
		person                       , 
		make_shared<string>(mail    ), 
		make_shared<string>(login   ), 
		make_shared<string>(password), 
		status                      ){}

User::User(Sptr<Person> person, Sptr<string> mail, Sptr<string> login, Sptr<string> password, bool status)
	:   mail    (mail    ),
	    login   (login   ),
	    person  (person  ),
	    status  (status  ),
	    password(password){}

Sptr<string> User::getMail    () { return mail    ;}
Sptr<string> User::getLogin   () { return login   ;}
bool         User::getStatus  () { return status  ;}
Sptr<Person> User::getPerson  () { return person  ;}
Sptr<string> User::getPassword() { return password;}

void User::setMail    (Sptr<string> mail    ){ this->mail     = mail    ;}
void User::setLogin   (Sptr<string> login   ){ this->login    = login   ;}
void User::setStatus  (bool         status  ){ this->status   = status  ;}
void User::setPerson  (Sptr<Person> person  ){ this->person   = person  ;}
void User::setPassword(Sptr<string> password){ this->password = password;}

}}
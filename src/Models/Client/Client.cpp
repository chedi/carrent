#include "Client.h"

namespace Models  {
namespace Clients {


Client::Client(Sptr<Client> client)
	: Client(
		client->person        ,
		client->driver_licence,
		client->age           ){}

Client::Client(Sptr<Person> person, string&& driver_licence, int age)
	: Client(
		person                             ,
		make_shared<string>(driver_licence),
		age                               ){}

Client::Client(Sptr<Person> person, Sptr<string> driver_licence, int age)
	:   age           (age           ),
	    person        (person        ),
	    driver_licence(driver_licence){}

void Client::setAge          (int          age           ){ this->age            = age            ;}
void Client::setPerson       (Sptr<Person> person        ){ this->person         = person         ;}
void Client::setDriverLicence(Sptr<string> driver_licence){ this->driver_licence = driver_licence ;}

int          Client::getAge          (){ return age           ;}
Sptr<Person> Client::getPerson       (){ return person        ;}
Sptr<string> Client::getDriverLicence(){ return driver_licence;}

}}
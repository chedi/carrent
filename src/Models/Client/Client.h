#ifndef CLIENT_H
#define CLIENT_H

#include <string>
#include <memory>

#include <Common/Common.h>
#include <Models/Common/Person.h>

using Models::Commons::Person;

namespace Models  {
namespace Clients {

class Client {
		int          age           ;
		Sptr<Person> person        ;
		Sptr<string> driver_licence;

	public:
		Client(      Client&&                   ) = default;
		Client(const Client&                    ) = default;
		Client(Sptr <Client>                    );

		Client(Sptr <Person>,      string&&, int);
		Client(Sptr <Person>, Sptr<string> , int);

		int          getAge          ();
		Sptr<Person> getPerson       ();
		Sptr<string> getDriverLicence();

		void setAge          (int         );
		void setPerson       (Sptr<Person>);
		void setDriverLicence(Sptr<string>);
};

}}

#endif
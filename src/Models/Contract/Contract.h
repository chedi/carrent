#ifndef MODEL_CONTRACT_H
#define MODEL_CONTRACT_H

#include <Common/Common.h>
#include <Models/Client/Client.h>
#include <Models/Contract/Lease.h>

using namespace Models::Clients;

namespace Models    {
namespace Contracts {

class Contract {
	Sptr<Lease > lease ;
	Sptr<Client> client;

public:
	Contract(      Contract&&) = default;
	Contract(const Contract& ) = default;
	Contract(Sptr <Contract> );

	Contract(Sptr <Lease>, Sptr <Client>);

	Sptr<Lease > getLease ();
	Sptr<Client> getClient();

	void setLease (Sptr<Lease >);
	void setClient(Sptr<Client>);
};		

}}

#endif
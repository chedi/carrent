#ifndef LEASE_STATUS_H
#define LEASE_STATUS_H

#include <map>
#include <string>

using namespace std;

namespace Models    {
namespace Contracts {

enum class LeaseStatus : unsigned char {
    ACTIVE         = 1,
    PENDING        = 2,
    ENDED_OK       = 3,
    STOPED_OK      = 4,
    ENDED_DISPUTE  = 5,
    STOPED_DISPUTE = 6
};

static const map<LeaseStatus, string> LEASE_STATUS_MESSAGES {
	{ LeaseStatus::ACTIVE        , "Active"            },
    { LeaseStatus::PENDING       , "Pending"           },
    { LeaseStatus::ENDED_OK      , "Ended ok"          },
    { LeaseStatus::STOPED_OK     , "Stoped ok"         },
    { LeaseStatus::ENDED_DISPUTE , "Ended in despute"  },
    { LeaseStatus::STOPED_DISPUTE, "Stoped in despute" }
};

}}

#endif
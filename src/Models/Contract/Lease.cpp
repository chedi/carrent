#include "Lease.h"

namespace Models    {
namespace Contracts {

Lease::Lease(Sptr<Lease> lease)
	: Lease(
		lease->car,
		lease->client,
		lease->start_date,
		lease->end_date,
		lease->status){}

Lease::Lease(Sptr<Car> car, Sptr<Client> client, ptime&& start_date, ptime&& end_date, LeaseStatus status)
	: Lease(
		car                                 ,
		client                              ,
		make_shared<ptime>(move(start_date)),
		make_shared<ptime>(move(end_date  )),
		status                             ){}

Lease::Lease(Sptr<Car> car, Sptr<Client> client, Sptr<ptime> start_date, Sptr<ptime> end_date, LeaseStatus status) 
	:   car       (car       ),
	    client    (client    ),
	    status    (status    ),
	    end_date  (end_date  ),
	    start_date(start_date){}

Sptr<Car>    Lease::getCar      () { return car        ;}
Sptr<Client> Lease::getClient   () { return client     ;}
LeaseStatus  Lease::getStatus   () { return status     ;}
Sptr<ptime>  Lease::getEndDate  () { return end_date   ;}
Sptr<ptime>  Lease::getStartDate() { return start_date ;}

void Lease::setCar      (Sptr<Car>    car       ){ this->car        = car        ;}
void Lease::setClient   (Sptr<Client> client    ){ this->client     = client     ;}
void Lease::setStatus   (LeaseStatus  status    ){ this->status     = status     ;}
void Lease::setEndDate  (Sptr<ptime>  end_date  ){ this->end_date   = end_date   ;}
void Lease::setStartDate(Sptr<ptime>  start_date){ this->start_date = start_date ;}

}}
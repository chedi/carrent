#include "Contract.h"

namespace Models    {
namespace Contracts {

Contract::Contract(Sptr<Contract> contract)
	: Contract(
		contract->lease ,
		contract->client){}

Contract::Contract(Sptr<Lease> leases, Sptr<Client> client)
	:   lease (lease ),
		client(client){}

Sptr<Lease > Contract::getLease (){ return lease  ;}
Sptr<Client> Contract::getClient(){ return client ;}

void Contract::setLease (Sptr<Lease>  lease ) { this->lease  = lease  ;}
void Contract::setClient(Sptr<Client> client) { this->client = client ;}

}}
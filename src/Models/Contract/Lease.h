#ifndef LEASE_H
#define LEASE_H

#include <memory>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <Common/Common.h>
#include <Models/Car/Car.h>
#include <Models/Client/Client.h>
#include <Models/Contract/LeaseStatus.h>

namespace Models    {
namespace Contracts {

using Models::Cars::Car       ;
using Models::Clients::Client ;
using boost::posix_time::ptime;

class Lease {
	Sptr<Car>    car       ;
	Sptr<Client> client    ;
	LeaseStatus  status    ;
	Sptr<ptime>  end_date  ;
	Sptr<ptime>  start_date;

public:
	Lease(      Lease&&                                                   ) = default;
	Lease(const Lease&                                                    ) = default;
	Lease(Sptr <Lease>                                                    );

	Lease(Sptr<Car>, Sptr<Client>,      ptime&&,      ptime&&, LeaseStatus);
	Lease(Sptr<Car>, Sptr<Client>, Sptr<ptime> , Sptr<ptime> , LeaseStatus);

	Sptr<Car>    getCar      ();
	Sptr<Client> getClient   ();
	LeaseStatus  getStatus   ();
	Sptr<ptime>  getEndDate  ();
	Sptr<ptime>  getStartDate();

	void setCar      (Sptr<Car>   );
	void setClient   (Sptr<Client>);
	void setStatus   (LeaseStatus );
	void setEndDate  (Sptr<ptime> );
	void setStartDate(Sptr<ptime> );
};

}}

#endif
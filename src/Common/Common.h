#ifndef COMMON_GLOBAL_H
#define COMMON_GLOBAL_H

#include <memory>
#include <vector>

#include <boost/mpl/int.hpp>
#include <boost/mpl/pair.hpp>

#include <boost/fusion/include/map.hpp>
#include <boost/fusion/include/pair.hpp>
#include <boost/fusion/include/at_key.hpp>
#include <boost/fusion/include/has_key.hpp>
#include <boost/fusion/include/boost_tuple.hpp>

using namespace std;

template <typename T> using Sptr   =                   shared_ptr<T>  ;
template <typename T> using VSptr  =            vector<shared_ptr<T>> ;
template <typename T> using SVptr  = shared_ptr<vector<T           >> ;
template <typename T> using SVSptr = shared_ptr<vector<shared_ptr<T>>>;


typedef boost::fusion::map<> NoDependencies     ;
typedef boost::fusion::map<> NoModelDependencies;

#endif